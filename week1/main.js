function random() {
  return Math.floor(Math.random() * 30);
}

let ranNum = random();
let count = 10;

const button = document.getElementById("guessButton");
const text = document.getElementById("message");
const btnNewGame = document.getElementById("newGame");

// Guess
button.addEventListener("click", function () {
  const numberClient = document.getElementById("number").value;

  count = count - 1;
  if (count < 0) return;

  if (numberClient == ranNum) {
    text.innerHTML = "You Win !!";
    text.style.backgroundColor = "green";
  } else if (numberClient < ranNum) {
    text.innerHTML = `Your guess was too low, guess higher. You have ${count} left`;
    text.style.backgroundColor = "red";
  } else {
    text.innerHTML = `Your guess was too higher, guess low. You have ${count} left`;
    text.style.backgroundColor = "red";
  }
});

// New Game
btnNewGame.addEventListener("click", function () {
  text.innerHTML = "";
  text.style.backgroundColor = "none";
  count = 10;
  ranNum = random();
});
